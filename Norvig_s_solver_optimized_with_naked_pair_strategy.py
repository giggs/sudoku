import time,itertools

### Rewrote Norvig's code after reading it and aimed for a simple list data structure rather than a dict, which is slightly faster.
### I also added the naked pairs strategy, which slows down the execution slightly (10%) on easier puzzles but speeds up the resolution
### of harder puzzle (up to 30%)

digits = '123456789'
squares = [x for x in range(81)]
rows = [[(y*9+x) for x in range(9)] for y in range(9)] 
cols = [[(y*9+x) for y in range(9)] for x in range(9)]
boxs = [[(y*9+x) for y in (section_y) for x in (section_x)] for section_y in ([0,1,2],[3,4,5],[6,7,8]) for section_x in ([0,1,2],[3,4,5],[6,7,8])]
all_containers = cols + rows + boxs
containers = {s: [container for container in all_containers if s in container] for s in squares}
peers = {s: set([p for container in containers[s] for p in container if p != s]) for s in squares}
common_peers = {frozenset((s1, s2)): set([p for p in peers[s1] if p in peers[s2]]) for s1,s2 in itertools.combinations(squares, 2) if s1 in peers[s2] or s2 in peers[s1]}

def print_grid(puzzle):
    grid = '-'*25+'\n'
    for y in range(9):
        for x in range(0,8,3):
            grid += '| ' + ' '.join(puzzle[9*y+x:9*y+x+3]) + ' '
        grid += '|\n'
        if y % 3 == 2: grid += '-'*25+'\n'
    return grid

def parse_grid(puzzle):
    values = [digits for s in squares]
    puzzle = puzzle.replace(' ','').replace('\n','')
    for s,d in enumerate(puzzle):
        if d in digits and not set_value(values, s, d):
            return False
    return values

def set_value(values, square, digit):
    other_digits = values[square].replace(digit, '')
    if all(remove_candidates(values, square, o_d) for o_d in other_digits): 
        return values
    else:
        return False ## If removing the candidates lead to a contradiction

def remove_candidates(values, square, digit):
    if digit not in values[square]:
        return values ## No changes
    values[square] = values[square].replace(digit,'')
    if not values[square]: 
        return False ## No possible value
    elif len(values[square]) == 1: ## Another digit being set to the square
        if not all(remove_candidates(values, peer, values[square]) for peer in peers[square]):
            return False ## If contradiction when removing the digit from peers
    elif len(values[square]) == 2: ## Naked pairs
        for square2 in peers[square]:
            if values[square] == values[square2]:
                for other_digit in values[square]:
                    if not all(remove_candidates(values, peer, other_digit) for peer in common_peers[frozenset((square, square2))]):
                        return False
    for container in containers[square]:
        digit_candidate_locations = [s for s in container if digit in values[s]]
        if not digit_candidate_locations: 
            return False ## No possible location
        elif len(digit_candidate_locations) == 1:
            if not set_value(values, digit_candidate_locations[0], digit):
                return False
    return values

def depth_first_search(puzzle):
    if not puzzle:
        return False ## Failure from previous hypothesis
    if sum(len(s) for s in puzzle) == 81:
        return puzzle 
    choices, square = min((len(puzzle[square]), square) for square in squares if len(puzzle[square]) > 1)
    for attempt in (depth_first_search(set_value(puzzle.copy(), square, digit)) for digit in puzzle[square]):
        if attempt: return attempt
    else: return False

def solve(puzzle):
    return depth_first_search(parse_grid(puzzle))


playing = 'y'
while playing == 'y':
    puzzle2, candidates, max_guess = '', {}, 0
    for _ in range(1,10):
        puzzle2 += input(f'Enter line {_}, blanks to be labeled as \'.\': ') + '\n'
    t0 = time.time()
    print(print_grid(solve(puzzle2)))
    t1 = time.time()
    print(f'Search time: {t1-t0}s') 
    playing = input('Play again? y/n')

# def solve_all(grids, name='', showif=0.0):
#     """Attempt to solve a sequence of grids. Report results.
#     When showif is a number of seconds, display puzzles that take longer.
#     When showif is None, don't display any puzzles."""
#     def time_solve(grid):
#         start = time.time()
#         values = solve(grid)
#         t = time.time()-start
#         # ## Display puzzles that take long enough
#         # if showif is not None and t > showif:
#         #     print_grid(grid_values(grid))
#         #     if values: display(values)
#         #     print('(%.2f seconds)\n' % t)
#         return (t, solved(values))
#     times, results = zip(*[time_solve(grid) for grid in grids])
#     N = len(grids)
#     if N > 1:
#         print("Solved %d of %d %s puzzles (avg %.4f secs (%d Hz), max %.4f secs)." % (
#             sum(results), N, name, sum(times)/N, N/sum(times), max(times)))

# def solved(values):
#     "A puzzle is solved if each unit is a permutation of the digits 1 to 9."
#     def unitsolved(unit): return set(values[s] for s in unit) == set(digits)
#     return values is not False and all(unitsolved(unit) for unit in all_containers)

# def from_file(filename, sep='\n'):
#     "Parse a file into a list of strings, separated by sep."
#     return open(filename).read().strip().split(sep)
# import random

# def shuffled(seq):
#     "Return a randomly shuffled copy of the input sequence."
#     seq = list(seq)
#     random.shuffle(seq)
#     return seq

# def random_puzzle(N=17):
#     """Make a random puzzle with N or more assignments. Restart on contradictions.
#     Note the resulting puzzle is not guaranteed to be solvable, but empirically
#     about 99.8% of them are solvable. Some have multiple solutions."""
#     values = dict((s, digits) for s in squares)
#     for s in shuffled(squares):
#         if not set_value(values, s, random.choice(values[s])):
#             break
#         ds = [values[s] for s in squares if len(values[s]) == 1]
#         if len(ds) >= N and len(set(ds)) >= 8:
#             return ''.join(values[s] if len(values[s])==1 else '.' for s in squares)
#     return random_puzzle(N) ## Give up and make a new puzzle

# for _ in range(5):
#     solve_all(from_file("easy_m.txt"), "easy", None)
#     solve_all(from_file("hard.txt"), "hard", None)
#     solve_all(from_file("hardest.txt"), "hardest", None)
#     solve_all([random_puzzle() for _ in range(99)], "random", 100.0)
#     print('')
# input()

