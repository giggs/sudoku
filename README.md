# Sudoku



This repository contains Sudoku solvers and a Sudoku grid parser + solver.   
The solver and the parser-solver combo with Norvig's name in them contain modified code from [Peter Norvig's solver](http://norvig.com/sudoku.html) and are subject to the MIT License.  
You can use those get the solution of a Sudoku grid from a screenshot, picture or text string.    
You will also find an archive containing an executable file to use on Windows machines.   
A slow version of this program is available [here](https://darkgiggsxx.pythonanywhere.com). Most machines should be able to run the program faster than the PythonAnywhere server. 
You can read about the writing of this program [here](https://giggs.gitlab.io/blog/sudoku-grid-parser/).

## How to use  

The solvers open up a console where you type in the puzzle string.  
The parsers require having Tesseract-OCR installed and has best results with the traineddata available in the archive (tessdata-best from [here](https://github.com/tesseract-ocr/tessdata_best)).  
The executable file should have the tessdata folder in the same directory, and the traineddata file should be in it the tessdata folder.  
To parse a picture or screenshot you can:  
* Drag and drop a file on the .exe/.py
* Copy a file from your explorer window and open the .exe/.py
* Simply opening the .exe/.py with a screenshot in the clipboard  

If you open the program with text in your clipboard, the program will default to parsing the file 'skewed.png'

### Limits

Neither solver should fail solving any possible Sudoku.  
Things that might make the parser fail:  
* Incomplete outer grid  
* Low resolution  
* Watermarks or artifacts in the cells  
* Both black and white lines in the grid (for example, black outer grid, white inner grids)  

Pictures are expected to fail more often than screenshots because of noise.

