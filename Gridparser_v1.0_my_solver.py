import cv2
import numpy as np
from PIL import ImageGrab
from tesserocr import PyTessBaseAPI
from itertools import combinations
import time, copy, functools, sys

possible_numbers = set(list('123456789')) 
key_squares = [(0,0), (1,3), (2,6), (3,1), (4,4), (5,7), (6,2), (7,5), (8,8)] 

def euclidian_distance(point1, point2):
    '''Calcuates the euclidian distance between the point1 and point2
    used to calculate the length of the four sides of the square'''
    return np.sqrt((point1[0] - point2[0]) ** 2 + (point1[1] - point2[1]) ** 2)

def order_corner_points(corners):
    '''The points obtained from contours may not be in order because of the skewness  of the image, or
    because of the camera angle. This function returns a list of corners in the right order'''
    sort_corners = [[corner[0][0], corner[0][1]] for corner in corners]
    x, y = [], []

    for _,(i,j) in enumerate(sort_corners):
        x.append(i)
        y.append(j)
    centroid = [sum(x) / len(x), sum(y) / len(y)]

    for _, item in enumerate(sort_corners):
        if item[0] < centroid[0]:
            if item[1] < centroid[1]:
                top_left = item
            else:
                bottom_left = item
        elif item[0] > centroid[0]:
            if item[1] < centroid[1]:
                top_right = item
            else:
                bottom_right = item
    ordered_corners = [top_left, top_right, bottom_right, bottom_left]
    return np.array(ordered_corners, dtype="float32")

def image_preprocessing(image, corners):
    '''This function undertakes all the preprocessing of the image'''
    ordered_corners = order_corner_points(corners)
    top_left, top_right, bottom_right, bottom_left = ordered_corners

    width1 = euclidian_distance(bottom_right, bottom_left)
    width2 = euclidian_distance(top_right, top_left)
    width = max(int(width1), int(width2))
    
    # Because a sudoku is a square, you techically only need one dimension. This also seemed to
    # yield better results for the OCR for unknown reason

    # To find the matrix for warp perspective function we need dimensions and matrix parameters
    dimensions = np.array([[0, 0], [width, 0], [width, width],
                           [0, width]], dtype="float32")

    matrix = cv2.getPerspectiveTransform(ordered_corners, dimensions)
    transformed_image = cv2.warpPerspective(image, matrix, (width, width))
    transformed_image = cv2.resize(transformed_image, (400, 400), interpolation=cv2.INTER_AREA)
    return transformed_image

def get_square_box_from_image(image):
    ''' This function returns the top-down view of the puzzle in grayscale.'''
    global big
    if image.shape[1] > 1000: big = True
    else: big = False
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    if gray.mean() < 80: # Detect if the grid has a dark background. It most likely has a white grid, so invert it.
        gray = cv2.threshold(gray,gray.mean(),255,cv2.THRESH_BINARY_INV)[1]
    blur = cv2.GaussianBlur(gray, (5,5), 0)
    adaptive_threshold = cv2.adaptiveThreshold(blur, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 11, 3)
    corners = cv2.findContours(adaptive_threshold, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    corners = corners[0] if len(corners) == 2 else corners[1]
    corners = sorted(corners, key=cv2.contourArea, reverse=True)
    corner = corners[0]
    length = cv2.arcLength(corner, True)
    approx = cv2.approxPolyDP(corner, 0.015 * length, True)
    puzzle_image = image_preprocessing(image, approx)

    return puzzle_image

def get_hough_lines(pic):
    '''Detect all the horizontal and vertical lines in the grid.
    If you do not detect 10 of each, report an error. Collect the coordinates of each
    to define the cell coordinates'''
    global answer
    answer = pic
    gray = cv2.cvtColor(pic,cv2.COLOR_BGR2GRAY)
    if gray.mean() < 80: # If the grid has a dark background (most likely white lines), invert it.
        gray = cv2.threshold(gray,gray.mean(),255,cv2.THRESH_BINARY_INV)[1]
        blur = cv2.GaussianBlur(gray,(5,5),0)
    else:
        blur = cv2.GaussianBlur(gray, (5,5), 0)
    adaptive_threshold = cv2.adaptiveThreshold(blur, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 7,2)
    edges = cv2.Canny(adaptive_threshold,140,255,apertureSize = 3)

    limit = 210
    filtered_lines = []

    while len(filtered_lines) < 20 and limit > 0: 
        # Because different images will have a different threshold limit to detect the lines, lower it until it is low enough.
        # Starting too low isn't good because you'll get too much noise
        limit -= 10
        lines = cv2.HoughLines(edges,1,np.pi/180,limit)

        if not lines.any():
            print('No lines were found');exit()

        theta_threshold = 0.05
        rho_threshold = 30 
        # Given a perfectly regular grid, no cell should be larger than 44 pixels. 30 is sufficient margin of error to not detect lines in the middle of cells in the case of noise like Moire

        # How many lines are similar to a given one
        similar_lines = {i : [] for i in range(len(lines))}
        for i in range(len(lines)):
            for j in range(len(lines)): # You might think this is not necessary. Why not range(i,len(lines)) ? This messes up the number of similar lines. See below
                if i == j:
                    continue
                rho_i,theta_i = lines[i][0]
                rho_j,theta_j = lines[j][0]
                if abs(rho_i - rho_j) < rho_threshold and abs(theta_i - theta_j) < theta_threshold:
                    similar_lines[i].append(j)

        # ordering the indices of the lines by how many are similar to them
        indices = [i for i in range(len(lines))]
        indices.sort(key=lambda x : len(similar_lines[x]))

        line_flags = len(lines)*[True]
        for i in range(len(lines) - 1):
            if not line_flags[indices[i]]: # If we already disregarded the ith element in the ordered list then we don't care
                continue
            rho_i,theta_i = lines[indices[i]][0]
            
            if 0.02 < theta_i < 1.54 or theta_i > 1.59: # Discard lines that aren't horizontal or vertical
                line_flags[indices[i]] = False
                for j in similar_lines[indices[i]]:
                    line_flags[j] = False

            else:
                for j in similar_lines[indices[i]]:
                    if indices.index(j) > i: # Only consider the elements that had less similar lines. This way, you save the line at the "center" of the blob
                        line_flags[j] = False             

        filtered_lines = []

        for i in range(len(lines)): 
            if line_flags[i]:
                filtered_lines.append(lines[i])

    vertical_lines, horizontal_lines = [], []

    for line in filtered_lines:
        rho,theta = line[0]
        a = np.cos(theta)
        b = np.sin(theta)
        x0 = a*rho
        y0 = b*rho
        x1 = int(x0 + 1000*(-b))
        y1 = int(y0 + 1000*(a))
        x2 = int(x0 - 1000*(-b))
        y2 = int(y0 - 1000*(a))
        if theta < 0.02:
            vertical_lines.append((x1+x2)//2)
            # cv2.line(pic,(x1,y1),(x2,y2),(0,0,255),2)
        else:
            horizontal_lines.append((y1+y2)//2)    
            # cv2.line(pic,(x1,y1),(x2,y2),(0,0,255),2)

    # cv2.imwrite('hough.jpg',pic)
    horizontal_lines = sorted(horizontal_lines)
    vertical_lines = sorted(vertical_lines)
    
    return [horizontal_lines, vertical_lines, gray]

def recognize_digits(hough_output):
    '''Detect squares that have a number in them, do additionnal image processing and send it to Tesseract for OCR
    Return a puzzle string and the list of the coordinates for where to write a number in each cell'''
    indexes = {}
    n = -1
    horizontal_lines, vertical_lines, gray = hough_output
    if len(horizontal_lines) != 10 or len(vertical_lines) != 10:
        puzzle = input('Failed to properly detect the lines. Enter the puzzle manually, row after row, with 0 or . for blanks')
        return puzzle, indexes

    puzzle = ''
    with PyTessBaseAPI(psm = 10) as api:
        api.SetVariable('tessedit_char_whitelist', '0123456789')
        for j,x in enumerate(horizontal_lines[:-1]):
            x1 = horizontal_lines[j+1]
            for i,y in enumerate(vertical_lines[:-1]):
                n += 1
                y1 = vertical_lines[i+1]
                square = gray[x:x1, y:y1]
                center = square[10:-10,10:-10]

                criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 5, 1.0)
                K = 2
                # Do color quantization on the center, aiming for 2 colours maximum.
                Z = center.reshape((-1,1))
                Z = np.float32(Z)
                _, label, c = cv2.kmeans(Z,K,None,criteria,4,cv2.KMEANS_RANDOM_CENTERS)
                c = np.uint8(c)      
                res = c[label.flatten()]
                center = res.reshape((center.shape))
        
                text = ''

                if c.max() - c.min() > 20: # Two colours in the center, so there's a digit
                    blur = cv2.GaussianBlur(square,(5,5),0)
                    oh,ow = square.shape[:2]
                    
                    if center[0][0] == c.min(): # Should handle white grid with colored cells with white number in them
                        square = cv2.threshold(square,center.mean()+10,255,cv2.THRESH_BINARY_INV)[1]
                        blur = square.copy()
                    
                    thresh = cv2.adaptiveThreshold(blur, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 11, 3)
                    contours = cv2.findContours(thresh, cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
                    contours = contours[0] if len(contours) == 2 else contours[1]
                    for cont in contours:
                        x2,y2,w,h = cv2.boundingRect(cont)
                        if (ow//7 <= w < ow-3) and (oh//3 <= h < oh-3):
                            square = square[y2:y2+h,x2:x2+w]
                            # cv2.rectangle(thresh, (x2, y2), (x2 + w, y2 + h), (255,255,255), 2) 
                            break
                    else:
                        square = square[8:-8,8:-8]
                    fill = center.max()

                    # Create new image for OCR to parse                    
                    height,width = square.shape[:2]
                    nh, nw = max(55,height*2), max(55,height*2)
                    blank = fill*np.ones(shape = (nh,nw), dtype=np.uint8)
                    height_offset = (nh-height)//2 +1
                    width_offset = (nw-width)//2 
                    blank[height_offset:height_offset+height,width_offset:width_offset+width] = square
                    square = blank
                    
                    # Do color quantization on it to get rid of Moire artifacts or similar
                    Z = square.reshape((-1,1))
                    Z = np.float32(Z)
                    ret, label, c = cv2.kmeans(Z,K,None,criteria,4,cv2.KMEANS_RANDOM_CENTERS)
                    c = np.uint8(c)
                    res = c[label.flatten()]
                    square = res.reshape((square.shape))
                    global big
                    if big:
                        square = cv2.GaussianBlur(square,(5,5),0) # This blur kernel seemed to give the best results on pictures, which had a bigger resolution.
                    else:
                        square = cv2.GaussianBlur(square,(3,3),0)

                    while (text == '' and square.any()) or ('0' in text and square.any()) or len(text) > 2:
                        api.SetImageBytes(square.tobytes(), square.shape[1], square.shape[0], 1, square.shape[1])
                        text = api.GetUTF8Text()
                        square = square[1:-1,1:-1] # If OCR failed, usually zooming in will work

                else:
                    text = '.'
                    indexes[n] = (x,y)
                
                puzzle += text.replace('\n','')

    return puzzle, indexes

def tests():
    assert recognize_digits(get_hough_lines(get_square_box_from_image(cv2.imread('success1.PNG'))))[0] == '..8....1.9....2...24....97.3.5.9...7...463...1...5.6.3.92....45...7....9.1....7..'
    assert recognize_digits(get_hough_lines(get_square_box_from_image(cv2.imread('success2.PNG'))))[0] == '7..5.2..9.681.453.3.......1..5.4.7......6....849...265.967.312.5..921..623..8..57'
    assert recognize_digits(get_hough_lines(get_square_box_from_image(cv2.imread('stuff.png'))))[0] == '...1.6.2..63...174..2..4.6..57..2631....5....8314..59..2.6..8..648...75..1.7.8...'
    assert recognize_digits(get_hough_lines(get_square_box_from_image(cv2.imread('grid.png'))))[0] == '39.2651.8.....47.548597.3.2.51.2..3......6..997.....24...19...3.496.3...1......5.'
    assert recognize_digits(get_hough_lines(get_square_box_from_image(cv2.imread('success3.png'))))[0] == '5.3.....44.85...9..7.4..8...54.1.93.9..3.5..7.32.8.46...1..6.4..4...17.93.....6.2'
    assert recognize_digits(get_hough_lines(get_square_box_from_image(cv2.imread('success4.png'))))[0] == '7.8....6.3.....2....531.497....2698..8.9.1.4..9278....237.695....4.....9.6....8.3'
    assert recognize_digits(get_hough_lines(get_square_box_from_image(cv2.imread('skewed.png'))))[0] == '936...2......9374..4.821..9472.....6...759...1.....3785..416.2..2137......4...157'
    assert recognize_digits(get_hough_lines(get_square_box_from_image(cv2.imread('inverted.png'))))[0] == '.3..........195.....8....6.8...6....4..8....1....2.....6....28....419..5.......7.'
    assert recognize_digits(get_hough_lines(get_square_box_from_image(cv2.imread('Sam1.png'))))[0] == '8...7...2..64.28...5..3..1..1.5.4.2.2.5...6.7.3.2.7.5..2..1..3...97.34..3...4...8'
    assert recognize_digits(get_hough_lines(get_square_box_from_image(cv2.imread('fail 2.png'))))[0] == '.....49...5.8.73....2.....8..1..35...3..6.4....84........7...26..5...1.99........'
    assert recognize_digits(get_hough_lines(get_square_box_from_image(cv2.imread('hard_grid.jpg'))))[0] == '34.....7.8..4.725.7.68..3.9.13..64....7..4.1...4...6.3.7965.1.2...7..598.3.2917..'
    assert recognize_digits(get_hough_lines(get_square_box_from_image(cv2.imread('1.jpg'))))[0] == '.31..4..6..4.3...92.8.56....7.3..1....94.85....3..2.9....62.3.73...4.9..9..5..82.'
    assert recognize_digits(get_hough_lines(get_square_box_from_image(cv2.imread('2.jpg'))))[0] == '...1.2...1.7.5.3.9..84.95...5.....8.74.2.8.35..3...6..5...4...86...2...3...9.7...'
    return 'tests pass'

def grab_image():
    try:
        original = cv2.imread(sys.argv[1])
    except:
        original = ImageGrab.grabclipboard()
        try:
            isinstance(original, bytes)
            original = cv2.cvtColor(np.array(original),cv2.COLOR_RGB2BGR)
        except:
            if isinstance(original, list):
                original = cv2.imread(original[0])
            else:
                original = cv2.imread('skewed.png')
    
    return original

def print_grid(puzzle):
    if not puzzle: 
        return 'Either the puzzle is impossible, you entered it wrong or you found a glitch!'
    
    grid = '-'*25+'\n'
    for y in range(9):
        for x in range(0,8,3):
            grid += '| ' + ' '.join(puzzle[9*y+x:9*y+x+3]) + ' '
        grid += '|\n'
        if y % 3 == 2: grid += '-'*25+'\n'
    return grid

def grid(puzzle):
    """Returns a 2D array represnting the grid with '.' for blanks"""
    sudoku_grid = []
    line = []
    for i,c in enumerate(puzzle.replace(' ','').replace('0', '.')):
        line.append(c)
        if i%9 == 8:
            sudoku_grid.append(list(line))
            line = []
    return sudoku_grid


@functools.cache
def get_influencers(y, x):
    """Returns a list of coordinates of all squares directly involved with the considered square"""
    influencers = []
    for f in get_row_neighbours, get_column_neighbours, get_square_neighbours: 
        influencers += f(y, x)
    return influencers

@functools.cache
def get_row_neighbours(y, x, width = 9):
    """Returns a list of coordinates for the other squares in a row"""
    return [(y,i) for i in range(width) if i != x]

@functools.cache
def get_column_neighbours(y, x, height = 9):
    """Returns a list of coordinates for the other squares in a column"""
    return [(j,x) for j in range(height) if j != y]

@functools.cache
def get_square_neighbours(y, x, height = 9, width = 9):
    """Returns a list of coordinates for the other squares in a small square"""
    x_range, y_range = (x // 3) * 3 , (y // 3) * 3 
    return [(j+y_range, i+x_range) for j in range(3) for i in range(3) if not (i+x_range == x and j+y_range == y)]

def get_regions():
    """Return a list of all rows, columns, 3x3 squares
    as well as a list of just the 3x3 squares"""
    regions, squares = [], []
    for (y,x) in key_squares:
        for f in get_row_neighbours, get_column_neighbours, get_square_neighbours:
            region = f(y,x) + [(y,x)]
            regions.append(region)
    return regions

all_regions = get_regions()

def initialize(puzzle, height, width, candidates):
    """Write down every candidates for every single square on the board
    If there's only one candidate, fill that square with the number"""
    for y in range(height):
        for x in range(width):
            if puzzle[y][x] == '.':
                candidates[(y, x)] = possible_numbers.copy()
    return candidates

def single_candidate(puzzle, candidates, changed = False):
    """Write down every candidates for every single square on the board
    If there's only one candidate, fill that square with the number"""
    deletion_list = []
    for (y,x) in candidates:
        influencers = set([puzzle[j][i] for (j,i) in get_influencers(y,x) if puzzle[j][i] != '.'])
        candidates[(y,x)] -= influencers
        if len(candidates[(y,x)]) == 1:
            puzzle[y][x] = list(candidates[(y,x)])[0]
            changed = True; deletion_list.append((y, x))
    for square in deletion_list: del candidates[square]
    return puzzle, candidates, changed

def single_position(puzzle, candidates, changed = False):
    """Within a row, column or 3x3 square, look for a number that has only one candidate.
    Fill the square that has this candidate with the number"""
    for region in all_regions:
        found = set([puzzle[y][x] for (y, x) in region if puzzle[y][x] != '.'])
        searching = possible_numbers - found
        region_candidates = [candidates[(y, x)] for (y, x) in region if puzzle[y][x] == '.']
        for number in searching:
            if sum([list(region_candidate).count(number) for region_candidate in region_candidates])  == 1:
                for (y, x) in region:
                    if puzzle[y][x] == '.' and number in candidates[(y, x)]:
                        puzzle[y][x] = number
                        changed = True; del candidates[(y, x)]
    return puzzle, candidates, changed

def naked_groups(candidates, changed = False):
    """In the case of naked groups, remove candidates from squares
    that aren't in the group"""
    for region in all_regions:
        coords = [(y, x) for (y, x) in region if (y, x) in candidates]
        for i in [2,3]:
            n_uples = [set(p) for p in combinations(coords, i)]
            for n_uple in n_uples:
                numbers = set()
                for coord in n_uple:
                    numbers |= candidates[coord]
                if len(numbers) == i:
                    for (y, x) in coords:
                        if (y, x) not in n_uple and (candidates[(y, x)] - numbers) != candidates[(y, x)]:
                            candidates[(y, x)] -= numbers; changed = True
    return candidates, changed         

def is_solved(puzzle):
    """Return whether a grid is solved"""
    return all([set(''.join([puzzle[y][x] for (y,x) in region])) == possible_numbers for region in all_regions])

def solve(puzzle, candidates, height = 9, width = 9, changed = True, guessing = 0):
    candidates = initialize(puzzle, height, width, candidates)
    while changed:
        changed = False
        puzzle, candidates, changed = single_candidate(puzzle, candidates, changed)
        puzzle, candidates, changed = single_position(puzzle, candidates, changed)
        if not changed: candidates, changed = naked_groups(candidates, changed)

    if not is_solved(puzzle): 
        if any(len(candidates[(y, x)]) == 0 for (y, x) in candidates) or not candidates:
            if not guessing: print('This puzzle cannot be solved')
            else: return False
        choose = min(candidates.items(), key = lambda x:len(x[1]))
        y, x, choices = choose[0][0], choose[0][1], list(choose[1])
        for choice in choices: 
            puzzle_copy, changed_copy = copy.deepcopy(puzzle), True
            puzzle_copy[y][x] = choice
            puzzle_copy = solve(puzzle_copy, {}, height, width, changed_copy, guessing+1)
            if puzzle_copy: 
                if is_solved(puzzle_copy): 
                    return puzzle_copy

    return puzzle

def show_results(puzzle, indexes):
    if not puzzle: 
        puzzle = input('I failed to solve, most likely bad digit recognition. Enter the puzzle manually, row after row, with 0 or . for blanks')
        puzzle = solve(grid(puzzle),{})
        indexes = {}
    if not indexes:
        return print_grid(puzzle)
    else:
        for cell,(x,y) in indexes.items():
            j,i = cell//9, cell%9
            digit_colour = [x[0] for x in cv2.bitwise_not(answer[y,x]).tolist()]
            digit_colour = (0,255,0) if (digit_colour[0] < 50 and digit_colour[1] < 50 and digit_colour[2] > 200) else (0,0,255)
            cv2.putText(answer,puzzle[j][i],(y+13,x+35),cv2.FONT_HERSHEY_SIMPLEX,1,digit_colour,1,cv2.LINE_AA)
        cv2.imshow('show', answer)
        cv2.waitKey(0)
        cv2.destroyAllWindows

height,width = 9,9
t = time.time()
sudoku = grab_image()
print(f'image grab time: {(time.time() - t):.3f}s')
t0 = time.time()
# print(tests())
puzzle,indexes = recognize_digits(get_hough_lines(get_square_box_from_image(sudoku)))
print(print_grid(puzzle))
print(f'grid parse time: {(time.time() - t0):.3f}s')

t1 = time.time()
puzzle = solve(grid(puzzle),{})
print(f'solve time: {(time.time()-t1):.3f}s')

show_results(puzzle, indexes)
input()