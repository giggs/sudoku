import time, functools, itertools, copy
    
possible_numbers = set(list('123456789')) 
key_squares = [(0,0), (1,3), (2,6), (3,1), (4,4), (5,7), (6,2), (7,5), (8,8)] 
candidates = {}

def grid(puzzle):
    """Returns a 2D array represnting the grid with '.' for blanks"""
    sudoku_grid = []
    for line in puzzle.replace(' ','').replace('0', '.').splitlines():
        sudoku_grid.append(list(line))
    return sudoku_grid

def show_grid(grid):
    """Returns a print-friendly string of the grid"""
    printed_grid = '-'*25+'\n'
    for j,line in enumerate(grid,1):
        for i in range(0,8,3):
            printed_grid += '| ' + ' '.join(line[i:i+3]) + ' '
        printed_grid += '|\n'
        if j % 3 == 0:
            printed_grid += '-'*25+'\n'
    return printed_grid    

@functools.cache
def get_influencers(y, x):
    """Returns a list of coordinates of all squares directly involved with the considered square"""
    influencers = []
    for f in get_row_neighbours, get_column_neighbours, get_square_neighbours: 
        influencers += f(y, x)
    return influencers

@functools.cache
def get_row_neighbours(y, x, width = 9):
    """Returns a list of coordinates for the other squares in a row"""
    return [(y,i) for i in range(width) if i != x]

@functools.cache
def get_column_neighbours(y, x, height = 9):
    """Returns a list of coordinates for the other squares in a column"""
    return [(j,x) for j in range(height) if j != y]

@functools.cache
def get_square_neighbours(y, x, height = 9, width = 9):
    """Returns a list of coordinates for the other squares in a small square"""
    x_range, y_range = (x // 3) * 3 , (y // 3) * 3 
    return [(j+y_range, i+x_range) for j in range(3) for i in range(3) if not (i+x_range == x and j+y_range == y)]

def get_regions():
    """Return a list of all rows, columns, 3x3 squares
    as well as a list of just the 3x3 squares"""
    regions, squares = [], []
    for (y,x) in key_squares:
        for f in get_row_neighbours, get_column_neighbours, get_square_neighbours:
            region = f(y,x) + [(y,x)]
            regions.append(region)
    return regions

all_regions = get_regions()

def initialize(puzzle, height, width, candidates):
    """Write down every candidates for every single square on the board
    If there's only one candidate, fill that square with the number"""
    for y in range(height):
        for x in range(width):
            if puzzle[y][x] == '.':
                candidates[(y, x)] = possible_numbers.copy()
    return candidates

def single_candidate(puzzle, candidates, changed = False):
    """Write down every candidates for every single square on the board
    If there's only one candidate, fill that square with the number"""
    deletion_list = []
    for (y,x) in candidates:
        influencers = set([puzzle[j][i] for (j,i) in get_influencers(y,x) if puzzle[j][i] != '.'])
        candidates[(y,x)] -= influencers
        if len(candidates[(y,x)]) == 1:
            puzzle[y][x] = list(candidates[(y,x)])[0]
            changed = True; deletion_list.append((y, x))
    for square in deletion_list: del candidates[square]
    return puzzle, candidates, changed

def single_position(puzzle, candidates, changed = False):
    """Within a row, column or 3x3 square, look for a number that has only one candidate.
    Fill the square that has this candidate with the number"""
    for region in all_regions:
        found = set([puzzle[y][x] for (y, x) in region if puzzle[y][x] != '.'])
        searching = possible_numbers - found
        region_candidates = [candidates[(y, x)] for (y, x) in region if puzzle[y][x] == '.']
        for number in searching:
            if sum([list(region_candidate).count(number) for region_candidate in region_candidates])  == 1:
                for (y, x) in region:
                    if puzzle[y][x] == '.' and number in candidates[(y, x)]:
                        puzzle[y][x] = number
                        changed = True; del candidates[(y, x)]
    return puzzle, candidates, changed

def naked_groups(candidates, changed = False):
    """In the case of naked groups, remove candidates from squares
    that aren't in the group"""
    for region in all_regions:
        coords = [(y, x) for (y, x) in region if (y, x) in candidates]
        for i in [2,3]:
            n_uples = [set(p) for p in itertools.combinations(coords, i)]
            for n_uple in n_uples:
                numbers = set()
                for coord in n_uple:
                    numbers |= candidates[coord]
                if len(numbers) == i:
                    for (y, x) in coords:
                        if (y, x) not in n_uple and (candidates[(y, x)] - numbers) != candidates[(y, x)]:
                            candidates[(y, x)] -= numbers; changed = True
    return candidates, changed         

def is_solved(puzzle):
    """Return whether a grid is solved"""
    return all([set(''.join([puzzle[y][x] for (y,x) in region])) == possible_numbers for region in all_regions])

def solve(puzzle, candidates, height = 9, width = 9, changed = True, guessing = 0):
    candidates = initialize(puzzle, height, width, candidates)
    while changed:
        changed = False
        puzzle, candidates, changed = single_candidate(puzzle, candidates, changed)
        puzzle, candidates, changed = single_position(puzzle, candidates, changed)
        if not changed: candidates, changed = naked_groups(candidates, changed)

    if not is_solved(puzzle): 
        if any(len(candidates[(y, x)]) == 0 for (y, x) in candidates) or not candidates:
            if not guessing: print('This puzzle cannot be solved')
            else: return False
        choose = min(candidates.items(), key = lambda x:len(x[1]))
        y, x, choices = choose[0][0], choose[0][1], list(choose[1])
        for choice in choices: 
            puzzle_copy, changed_copy = copy.deepcopy(puzzle), True
            puzzle_copy[y][x] = choice
            puzzle_copy = solve(puzzle_copy, {}, height, width, changed_copy, guessing+1)
            if puzzle_copy: 
                if is_solved(puzzle_copy): 
                    return puzzle_copy

    return puzzle

playing = 'y'
while playing == 'y':
    puzzle = ''
    for _ in range(1,10):
        puzzle += input(f'Enter line {_}, blanks to be labeled as \'.\': ') + '\n'
    t0 = time.time()
    print(show_grid(solve(grid(puzzle), {})))
    t1 = time.time()
    print(f'Search time: {t1-t0}s') 
    playing = input('Play again? y/n')